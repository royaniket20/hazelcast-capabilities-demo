create Table SKILL (
ID VARCHAR(100) NOT NULL PRIMARY KEY,
NAME VARCHAR(100) NOT NULL
);

insert into SKILL (ID, NAME) values ('96d94a4d-c5b3-4e5a-b6a1-9b971034f527', 'NXP');
insert into SKILL (ID, NAME) values ('7e109a2b-a994-4a33-9705-deb559b88266', 'Culture Change');
insert into SKILL (ID, NAME) values ('e6dad29b-9625-449d-8038-e0a2c34198e0', 'HUD');
insert into SKILL (ID, NAME) values ('3baae33d-f786-4a4b-a16f-229277a349a7', 'NCLB');
insert into SKILL (ID, NAME) values ('1406396d-ea9d-48bc-acdf-286936182471', 'Ubuntu');