create Table COMPANY (
ID VARCHAR(100) NOT NULL PRIMARY KEY,
NAME VARCHAR(100) NOT NULL ,
ADDRESS VARCHAR(200) NOT NULL ,
INDUSTRY VARCHAR(100) NOT NULL
);


insert into COMPANY (ID, NAME, ADDRESS, INDUSTRY) values ('e97ed836-3bee-4a6b-8b49-d3b0f3235b4b', 'Tagcat', '3840 Calypso Drive', 'n/a');
insert into COMPANY (ID, NAME, ADDRESS, INDUSTRY) values ('72576add-85cd-43aa-97d8-c6e76b8626d0', 'Devcast', '5 Brickson Park Avenue', 'Electric Utilities: Central');
insert into COMPANY (ID, NAME, ADDRESS, INDUSTRY) values ('c48284f1-061f-4019-9f49-53697f9618ad', 'Fivechat', '3873 Hansons Drive', 'Steel/Iron Ore');
insert into COMPANY (ID, NAME, ADDRESS, INDUSTRY) values ('ddeeb6eb-ef31-4191-abc3-dcf21dea56b2', 'Skidoo', '4924 Mayer Junction', 'Finance: Consumer Services');
insert into COMPANY (ID, NAME, ADDRESS, INDUSTRY) values ('56e5bf23-815b-448d-bb15-3b035f7e45f0', 'Skimia', '6601 Brown Hill', 'n/a');
