package com.aniket.hazelcast.NumberOne.repository;

import com.aniket.hazelcast.NumberOne.entity.EmployeeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeRepo   extends JpaRepository<EmployeeEntity,String> {
}
