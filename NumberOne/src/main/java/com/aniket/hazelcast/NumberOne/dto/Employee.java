package com.aniket.hazelcast.NumberOne.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
    @AllArgsConstructor
    @NoArgsConstructor
    @Builder

    public class Employee implements Serializable {
    private static final long serialVersionUID = 1L;
        private String id;


        private String name;


        private Skill skill;

        private String address;

        private Long contactNum;
        private Company company;


    }