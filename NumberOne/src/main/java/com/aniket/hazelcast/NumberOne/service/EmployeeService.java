package com.aniket.hazelcast.NumberOne.service;

import com.aniket.hazelcast.NumberOne.dto.Employee;

import java.util.List;
public interface EmployeeService {

    public List<Employee>  getAllEmployees();

    public Employee  updateEmployeeById(String id , Employee employee);

    public Employee  getEmployeeById(String id);
}
