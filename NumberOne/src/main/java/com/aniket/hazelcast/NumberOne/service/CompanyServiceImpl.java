package com.aniket.hazelcast.NumberOne.service;

import com.aniket.hazelcast.NumberOne.dto.Company;
import com.aniket.hazelcast.NumberOne.dto.Employee;
import com.aniket.hazelcast.NumberOne.repository.CompanyRepo;
import com.aniket.hazelcast.NumberOne.repository.EmployeeRepo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CompanyServiceImpl implements  CompanyService{

    @Autowired
    private CompanyRepo companyRepo;


    @Override
    @Cacheable(value = "getRandomCompany" , key = "#compId",cacheResolver = "cacheResolver" )
    public Company getRandomCompany(String compId) {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        return  companyRepo.findById(compId).map(item->{
            Company company = new Company();

            BeanUtils.copyProperties(item,company);
            return company;
        }).get();

    }
}
