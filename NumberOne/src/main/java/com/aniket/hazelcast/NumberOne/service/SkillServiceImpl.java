package com.aniket.hazelcast.NumberOne.service;

import com.aniket.hazelcast.NumberOne.dto.Company;
import com.aniket.hazelcast.NumberOne.dto.Employee;
import com.aniket.hazelcast.NumberOne.dto.Skill;
import com.aniket.hazelcast.NumberOne.repository.CompanyRepo;
import com.aniket.hazelcast.NumberOne.repository.EmployeeRepo;
import com.aniket.hazelcast.NumberOne.repository.SkillRepo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class SkillServiceImpl implements  SkillService{

    @Autowired
    private SkillRepo skillRepo;


    @Override
    @Cacheable(value = "getRandomSkill" , key = "#skillId",cacheResolver = "cacheResolver" )
    public Skill getRandomSkill(String skillId) {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        return  skillRepo.findById(skillId).map(item->{
            Skill skill = new Skill();

            BeanUtils.copyProperties(item,skill);
            return skill;
        }).get();

    }
}
