package com.aniket.hazelcast.NumberOne.config;

import com.hazelcast.cluster.MembershipEvent;
import com.hazelcast.cluster.MembershipListener;
import com.hazelcast.core.LifecycleEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import static com.aniket.hazelcast.NumberOne.util.AppConstants.MEMBER_COUNT;

@Slf4j
@Component
public class ClusterMembershipListener implements MembershipListener {

    @Autowired
    private ApplicationContext applicationContext;

    public void memberAdded(MembershipEvent membershipEvent) {
        log.info("Member added to Hz Cluster : {}", membershipEvent);
        membershipEvent.getMembers().stream().forEach(item->{
            log.info("MEMBER AVAILABLE NOW POST ADDITION : {}",item);
        });
        log.info("SPECIFIC MEMBER ADDED : {}", membershipEvent.getMember());
        int memberCount = MEMBER_COUNT.incrementAndGet();
        log.info("Currently Number of Members available in Cluster - {}",memberCount);
        if(MEMBER_COUNT.get()>=2){
            log.info("As Number of Members goes Up threshold - Lets Switch back to Dist Cache");
            CustomCacheResolver contextBean = applicationContext.getBean(CustomCacheResolver.class);
            contextBean.getLifecycleEventConsumer().accept(new LifecycleEvent(LifecycleEvent.LifecycleState.CLIENT_CONNECTED));
        }
    }

    public void memberRemoved(MembershipEvent membershipEvent) {
        log.info("Member Removed from Hz Cluster : {}", membershipEvent);
        membershipEvent.getMembers().stream().forEach(item->{
            log.info("MEMBER REMAINING AFTER REMOVAL- {}",item);
        });
        log.info("SPECIFIC MEMBER REMOVED - {}", membershipEvent.getMember());
        int memberCount = MEMBER_COUNT.decrementAndGet();
        log.info("Currently Number of Members available in Cluster - {}",memberCount);
        if(MEMBER_COUNT.get()<2){
         log.info("As Number of Members goes Below threshold - Lets Switch back to Alternate Cache Until things are stable again");
            CustomCacheResolver contextBean = applicationContext.getBean(CustomCacheResolver.class);
            contextBean.getLifecycleEventConsumer().accept(new LifecycleEvent(LifecycleEvent.LifecycleState.CLIENT_DISCONNECTED));
        }
    }

}