package com.aniket.hazelcast.NumberOne.service;

import com.aniket.hazelcast.NumberOne.dto.Company;
import com.aniket.hazelcast.NumberOne.dto.Employee;
import com.aniket.hazelcast.NumberOne.dto.Skill;
import com.aniket.hazelcast.NumberOne.entity.CompanyEntity;
import com.aniket.hazelcast.NumberOne.entity.EmployeeEntity;
import com.aniket.hazelcast.NumberOne.entity.SkillEntity;
import com.aniket.hazelcast.NumberOne.repository.CompanyRepo;
import com.aniket.hazelcast.NumberOne.repository.EmployeeRepo;
import com.aniket.hazelcast.NumberOne.repository.SkillRepo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class EmployeeServiceImpl implements  EmployeeService{

    @Autowired
    private EmployeeRepo employeeRepo;

    @Autowired
    private SkillRepo skillRepo;

    @Autowired
    private CompanyRepo companyRepo;

    @Autowired
    private CacheManager cacheManager;



    @Override
    @Cacheable(value = "getAllEmployees" ,cacheResolver = "cacheResolver" )
    public List<Employee> getAllEmployees() {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        List<Employee> employees =   employeeRepo.findAll().stream().map(item->{
            Employee emp = new Employee();
            Company company = new Company();
            Skill skill = new Skill();

            BeanUtils.copyProperties(item,emp);
            SkillEntity skillEntity = skillRepo.findById(item.getSkillId()).get();
            CompanyEntity companyEntity = companyRepo.findById(item.getCompanyId()).get();
            BeanUtils.copyProperties(companyEntity,company);
            BeanUtils.copyProperties(skillEntity,skill);
            emp.setCompany(company);
            emp.setSkill(skill);
            return emp;
        }).collect(Collectors.toList());

        return employees;

    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = "getEmployeeById" , key = "#id",cacheResolver = "cacheResolver" )
    })
    @CacheEvict(value = "getAllEmployees", allEntries = true)
    public Employee updateEmployeeById(String id, Employee employee) {
        Cache getAllEmployees = cacheManager.getCache("getAllEmployees");
       log.info("Cache ----- {}",getAllEmployees);
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        EmployeeEntity employeeEntity = employeeRepo.findById(id).get();
        employeeEntity.setAddress(employee.getAddress());
        employeeEntity.setName(employee.getName());
        employeeEntity.setContactNum(employee.getContactNum());
        EmployeeEntity entity = employeeRepo.save(employeeEntity);
        Employee emp = new Employee();
        Company company = new Company();
        Skill skill = new Skill();

        BeanUtils.copyProperties(entity,emp);
        SkillEntity skillEntity = skillRepo.findById(entity.getSkillId()).get();
        CompanyEntity companyEntity = companyRepo.findById(entity.getCompanyId()).get();
        BeanUtils.copyProperties(companyEntity,company);
        BeanUtils.copyProperties(skillEntity,skill);
        emp.setCompany(company);
        emp.setSkill(skill);
        return emp;
    }

    @Override
    @Cacheable(value = "getEmployeeById" , key = "#id",cacheResolver = "cacheResolver" )
    public Employee getEmployeeById(String id) {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        EmployeeEntity entity = employeeRepo.findById(id).get();
        Employee emp = new Employee();
        Company company = new Company();
        Skill skill = new Skill();

        BeanUtils.copyProperties(entity,emp);
        SkillEntity skillEntity = skillRepo.findById(entity.getSkillId()).get();
        CompanyEntity companyEntity = companyRepo.findById(entity.getCompanyId()).get();
        BeanUtils.copyProperties(companyEntity,company);
        BeanUtils.copyProperties(skillEntity,skill);
        emp.setCompany(company);
        emp.setSkill(skill);
        return emp;
    }
}
