package com.aniket.hazelcast.NumberOne.repository;

import com.aniket.hazelcast.NumberOne.entity.CompanyEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CompanyRepo extends JpaRepository<CompanyEntity,String> {
}
