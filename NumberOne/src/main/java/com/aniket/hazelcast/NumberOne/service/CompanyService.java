package com.aniket.hazelcast.NumberOne.service;

import com.aniket.hazelcast.NumberOne.dto.Company;
import com.aniket.hazelcast.NumberOne.dto.Employee;

import java.util.List;

public interface CompanyService {

    public Company getRandomCompany(String compId);
}
