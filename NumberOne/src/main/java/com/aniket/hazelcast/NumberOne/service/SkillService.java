package com.aniket.hazelcast.NumberOne.service;

import com.aniket.hazelcast.NumberOne.dto.Employee;
import com.aniket.hazelcast.NumberOne.dto.Skill;

import java.util.List;

public interface SkillService {

    public Skill getRandomSkill(String skillId);
}
