package com.aniket.hazelcast.NumberOne.util;

import java.util.concurrent.atomic.AtomicInteger;

public class AppConstants {

    public static final String APP_ONE_CACHE_getEmployeeById = "getEmployeeById";
    public static final String APP_ONE_CACHE_getAllEmployees = "getAllEmployees";

    public static final String APP_ONE_CACHE_getRandomSkill = "getRandomSkill";

    public static final String APP_ONE_CACHE_getRandomCompany = "getRandomCompany";

    public static final AtomicInteger MEMBER_COUNT = new AtomicInteger();
}
