package com.aniket.hazelcast.NumberOne;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class NumberOneApplication {

	public static void main(String[] args) {
		SpringApplication.run(NumberOneApplication.class, args);
	}

}
