package com.aniket.hazelcast.NumberOne.controller;

import com.aniket.hazelcast.NumberOne.dto.Employee;
import com.aniket.hazelcast.NumberOne.dto.SystemInfo;
import com.aniket.hazelcast.NumberOne.service.EmployeeService;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Date;

@RestController
@RequestMapping("/NumberOne")
public class NumberOneController {

    @Autowired
    private EmployeeService employeeService;

    @GetMapping("/allEmployee")
    public ObjectNode getAllEmployees() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setDateFormat(new SimpleDateFormat("dd-MM-yyyy"));
        ObjectNode rootNode = mapper.createObjectNode();
        SystemInfo systemInfo = systemInfo();
        rootNode.set("systemInfo", mapper.convertValue(systemInfo, JsonNode.class));
        rootNode.set("data", mapper.convertValue(employeeService.getAllEmployees(), JsonNode.class));
        return rootNode;


    }

    @GetMapping("/getEmployee/{id}")
    public ObjectNode getEmployee(@PathVariable String id) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setDateFormat(new SimpleDateFormat("dd-MM-yyyy"));
        ObjectNode rootNode = mapper.createObjectNode();
        SystemInfo systemInfo = systemInfo();
        rootNode.set("systemInfo", mapper.convertValue(systemInfo, JsonNode.class));
        rootNode.set("data", mapper.convertValue(employeeService.getEmployeeById(id), JsonNode.class));
        return rootNode;


    }

    @PutMapping("/updateEmployee/{id}")
    public ObjectNode updateEmployee(@PathVariable String id , @RequestBody Employee employee) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setDateFormat(new SimpleDateFormat("dd-MM-yyyy"));
        ObjectNode rootNode = mapper.createObjectNode();
        SystemInfo systemInfo = systemInfo();
        rootNode.set("systemInfo", mapper.convertValue(systemInfo, JsonNode.class));
        rootNode.set("data", mapper.convertValue(employeeService.updateEmployeeById(id,employee), JsonNode.class));
        return rootNode;


    }

    private SystemInfo systemInfo() {
        SystemInfo systemInfo = new SystemInfo();
        try {
            InetAddress ip = InetAddress.getLocalHost();
            systemInfo.setIpAddress(ip.toString());
            String hostname = ip.getHostName();
            systemInfo.setHostName(hostname);

            InetAddress localHost = InetAddress.getLocalHost();
            NetworkInterface ni = NetworkInterface.getByInetAddress(localHost);
            byte[] hardwareAddress = ni.getHardwareAddress();

            String[] hexadecimal = new String[hardwareAddress.length];
            for (int i = 0; i < hardwareAddress.length; i++) {
                hexadecimal[i] = String.format("%02X", hardwareAddress[i]);
            }
            String macAddress = String.join("-", hexadecimal);
            systemInfo.setMacAddress(macAddress);
        } catch (UnknownHostException | SocketException e) {
            systemInfo.setMacAddress("ERROR");
            systemInfo.setIpAddress("ERROR");
            systemInfo.setHostName("ERROR");
        } finally {
                systemInfo.setDate(new Date());
        }
        return systemInfo;
    }
}
