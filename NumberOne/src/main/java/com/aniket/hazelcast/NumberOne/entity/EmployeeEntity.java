package com.aniket.hazelcast.NumberOne.entity;


import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@DynamicInsert
@DynamicUpdate
@Table(name = "EMPLOYEE")
    public class EmployeeEntity {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(name = "ID")
        private String id;

    @Column(name = "NAME")
        private String name;

    @Column(name = "SKILL_ID")
        private String skillId;
    @Column(name = "ADDRESS")
        private String address;
    @Column(name = "CONTACT_NUM")
        private Long contactNum;
    @Column(name = "COMPANY_ID")
        private String companyId;

    }