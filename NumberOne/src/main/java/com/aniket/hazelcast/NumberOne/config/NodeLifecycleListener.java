package com.aniket.hazelcast.NumberOne.config;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.LifecycleEvent;
import com.hazelcast.core.LifecycleListener;
import com.hazelcast.map.IMap;
import com.hazelcast.spring.cache.HazelcastCacheManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import static com.aniket.hazelcast.NumberOne.util.AppConstants.*;

@Slf4j
@Component
public class NodeLifecycleListener implements LifecycleListener {


    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private HazelcastInstance hazelcastInstance;



    @Override
    public void stateChanged(LifecycleEvent event) {
        log.info("A NODE LIFECYCLE EVENT IS FIRED : {}", event);
        CustomCacheResolver contextBean = applicationContext.getBean(CustomCacheResolver.class);
        contextBean.getLifecycleEventConsumer().accept(event);
        if (event.getState().equals(LifecycleEvent.LifecycleState.CLIENT_CONNECTED)) {
            log.info("Client is connected to Cluster - RESET MEMBER COUNT");
            MEMBER_COUNT.set(this.hazelcastInstance.getCluster().getMembers().size());
           log.info("Lets clear all Necessary Cache for Be in Sync");
            hazelcastInstance.getMap(APP_ONE_CACHE_getAllEmployees).clear();
            hazelcastInstance.getMap(APP_ONE_CACHE_getEmployeeById).clear();
            hazelcastInstance.getMap(APP_ONE_CACHE_getRandomCompany).clear();
            hazelcastInstance.getMap(APP_ONE_CACHE_getRandomSkill).clear();
        }
    }


}
