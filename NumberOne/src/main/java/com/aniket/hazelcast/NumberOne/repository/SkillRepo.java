package com.aniket.hazelcast.NumberOne.repository;

import com.aniket.hazelcast.NumberOne.entity.SkillEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SkillRepo extends JpaRepository<SkillEntity,String> {
}
