package com.aniket.hazelcast.NumberTwo.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SystemInfo {
    private String ipAddress;
    private String hostName;

    private String macAddress;

    private Date date;

}
