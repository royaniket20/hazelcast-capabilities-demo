package com.aniket.hazelcast.NumberTwo.config;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.LifecycleEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.interceptor.CacheOperationInvocationContext;
import org.springframework.cache.interceptor.CacheResolver;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.function.Consumer;

@Slf4j
public class CustomCacheResolver implements CacheResolver {

    private final CacheManager cacheManager;
    private final CacheManager alternateCacheManager;

    private final HazelcastInstance hazelcastInstance;

    private final Consumer<LifecycleEvent> lifecycleEventConsumer = event -> {
        if (event.getState().equals(LifecycleEvent.LifecycleState.CLIENT_DISCONNECTED)) {
            log.info("Client is Disconnected from Cluster ------ >");
            isClientConnectedToCluster = Boolean.FALSE;
        } else if (event.getState().equals(LifecycleEvent.LifecycleState.CLIENT_CONNECTED)) {
            log.info("Client is connected to Cluster ------ >");
            isClientConnectedToCluster = Boolean.TRUE;
        }
    };

    public Consumer<LifecycleEvent> getLifecycleEventConsumer() {
        return lifecycleEventConsumer;
    }


    public void setClientConnectedToCluster(Boolean clientConnectedToCluster) {
        isClientConnectedToCluster = clientConnectedToCluster;
    }

    //This is Volatile So that Immediately available to all thread
    private volatile  Boolean isClientConnectedToCluster = Boolean.TRUE;

    public CustomCacheResolver(final CacheManager cacheManager, CacheManager alternateCacheManager, HazelcastInstance hazelcastInstance) {
        this.cacheManager = cacheManager;
        this.alternateCacheManager = alternateCacheManager;
        this.hazelcastInstance = hazelcastInstance;
        log.info("------ CACHE RESOLVER IS INITIATED ---------------");
    }

    @Override
    public Collection<? extends Cache> resolveCaches(CacheOperationInvocationContext<?> context) {
        Collection<String> cacheNames = getCacheNames(context);
        log.info("====Retrieved Cache names -------> {}", cacheNames);
        log.info("******************* Cache manager Stuff ******************");
        log.info(cacheManager.getClass().toString());
        log.info(alternateCacheManager.getClass().toString());
        if (cacheNames == null) {
            return Collections.emptyList();
        }
        Collection<Cache> result = new ArrayList<>(cacheNames.size());


        if (isClientConnectedToCluster) {
            for (String cacheName : cacheNames) {
                Cache cache = cacheManager.getCache(cacheName);
                if (cache == null) {
                    throw new IllegalArgumentException("Cannot find cache named '" +
                            cacheName + "' for " + context.getOperation());
                }
                result.add(cache);
            }
            log.info("CACHE OF HZ IS SELECTED  ---------------> {} ", result);
        } else {
            log.error("DISTRIBUTED CACHE CLUSTER IS NOW DOWN -----------------");
            log.info("FLIPPING TO NO OP CACHE UNTIL CACHE IS UP !!! ");
            for (String cacheName : cacheNames) {
                Cache cache = alternateCacheManager.getCache(cacheName);
                if (cache == null) {
                    throw new IllegalArgumentException("Cannot find cache named '" +
                            cacheName + "' for " + context.getOperation());
                }
                result.add(cache);
            }
            log.info("CACHE OF NO OP IS SELECTED  ---------------> {} ", result);
        }

        log.info("CACHE OF FINAL RESULT DONE  ---------------> {} ", result);
        return result;
    }

    protected Collection<String> getCacheNames(CacheOperationInvocationContext<?> context) {
        return context.getOperation().getCacheNames();
    }

}