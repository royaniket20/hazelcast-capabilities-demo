package com.aniket.hazelcast.NumberTwo.service;

import com.aniket.hazelcast.NumberTwo.dto.Company;
import com.aniket.hazelcast.NumberTwo.dto.Skill;
import com.aniket.hazelcast.NumberTwo.entity.CompanyEntity;
import com.aniket.hazelcast.NumberTwo.entity.SkillEntity;
import com.aniket.hazelcast.NumberTwo.repository.CompanyRepo;
import com.aniket.hazelcast.NumberTwo.repository.SkillRepo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class SkillServiceImpl implements  SkillService{

    @Autowired
    private SkillRepo skillRepo;

    @Override
    @Caching(evict = {
            @CacheEvict(value = "getRandomSkill" , key = "#skillId",cacheResolver = "cacheResolver" ),
            @CacheEvict(value = "getAllSkills", allEntries = true),
            @CacheEvict(value = "getAllEmployees", allEntries = true),
            @CacheEvict(value = "getEmployeeById", allEntries = true)
    })
    public Skill updateRandomSkill(String skillId, Skill skill) {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        SkillEntity skillEntity = skillRepo.findById(skillId).get();
        skillEntity.setName(skill.getName());
        skillRepo.save(skillEntity);
        Skill skl = new Skill();

        BeanUtils.copyProperties(skillEntity,skl);
        return skl;
    }

    @Override
    @Cacheable(value = "getAllSkills" ,cacheResolver = "cacheResolver" )
    public List<Skill> getAllSkills() {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        List<Skill> skills =   skillRepo.findAll().stream().map(item->{
            Skill skl = new Skill();
            BeanUtils.copyProperties(item,skl);
            return skl;
        }).collect(Collectors.toList());

        return skills;
    }
}
