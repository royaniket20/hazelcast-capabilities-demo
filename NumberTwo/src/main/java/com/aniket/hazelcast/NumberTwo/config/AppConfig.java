package com.aniket.hazelcast.NumberTwo.config;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.spring.cache.HazelcastCacheManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.cache.interceptor.CacheResolver;
import org.springframework.cache.support.NoOpCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.UUID;

import static com.aniket.hazelcast.NumberTwo.util.AppConstants.MEMBER_COUNT;


@Configuration
@Slf4j
public class AppConfig extends CachingConfigurerSupport {

    @Bean
    public CacheManager noOpCacheManager(){
        log.info("NO OP CACHE MANAGER IS INITIALIZED");
        return new NoOpCacheManager();
    }

    @Autowired
    private HazelcastInstance hazelcastInstance;

    @Autowired
    private NodeLifecycleListener nodeLifecycleListener;

    @Autowired
    private ClusterMembershipListener clusterMembershipListener;


    @Override
    @Bean
    public CacheManager cacheManager() {
        log.info("HZ CACHE MANAGER INITIALIZED"+ hazelcastInstance.toString());
        hazelcastInstance.getLifecycleService().addLifecycleListener( nodeLifecycleListener );
        log.info("HZ CACHE MANAGER IS ADDED TO LIFECYCLE LISTENER");
        hazelcastInstance.getCluster().addMembershipListener( clusterMembershipListener );
        log.info("HZ CACHE MANAGER IS ADDED TO CUSTER EVENT LISTENER");
         hazelcastInstance.getCluster().getMembers().forEach(item->{
             MEMBER_COUNT.incrementAndGet();
         });
         log.info("Total Number of Members Connected ---- : {}",MEMBER_COUNT.get());
        return new HazelcastCacheManager(hazelcastInstance);
    }



    @Override
    @Bean
    public CacheResolver cacheResolver() {
        CustomCacheResolver customCacheResolver =  new CustomCacheResolver(cacheManager(), noOpCacheManager(),hazelcastInstance);
        checkIfClusterIsAvailableOrNot(customCacheResolver);
        return customCacheResolver;
    }

    private void checkIfClusterIsAvailableOrNot(CustomCacheResolver customCacheResolver) {
      try {
          log.info("Is Hazelcast Client Connected to Server ? -- {}", hazelcastInstance.getMap(UUID.randomUUID().toString()));
          customCacheResolver.setClientConnectedToCluster(Boolean.TRUE);
      }catch (Exception ex){
          log.info("Client is Right Now not connected to Server ----> {}",ex.getMessage());
          customCacheResolver.setClientConnectedToCluster(Boolean.FALSE);
      }
    }
}
