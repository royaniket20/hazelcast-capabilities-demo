package com.aniket.hazelcast.NumberTwo.repository;


import com.aniket.hazelcast.NumberTwo.entity.CompanyEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CompanyRepo extends JpaRepository<CompanyEntity,String> {
}
