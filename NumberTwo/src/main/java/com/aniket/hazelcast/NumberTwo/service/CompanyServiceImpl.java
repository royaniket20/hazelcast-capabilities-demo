package com.aniket.hazelcast.NumberTwo.service;


import com.aniket.hazelcast.NumberTwo.dto.Company;
import com.aniket.hazelcast.NumberTwo.entity.CompanyEntity;
import com.aniket.hazelcast.NumberTwo.repository.CompanyRepo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CompanyServiceImpl implements  CompanyService{

    @Autowired
    private CompanyRepo companyRepo;
    @Override
    @Caching(evict = {
            @CacheEvict(value = "getRandomCompany" , key = "#compId",cacheResolver = "cacheResolver" ),
            @CacheEvict(value = "getAllCompanies", allEntries = true),
            @CacheEvict(value = "getAllEmployees", allEntries = true),
            @CacheEvict(value = "getEmployeeById", allEntries = true)
    })

    public Company updateCompany(String compId, Company company) {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        CompanyEntity companyEntity = companyRepo.findById(compId).get();
        companyEntity.setAddress(company.getAddress());
        companyEntity.setName(company.getName());
        companyEntity.setIndustry(company.getIndustry());
        companyRepo.save(companyEntity);
        Company comp = new Company();

        BeanUtils.copyProperties(companyEntity,comp);
        return comp;
    }

    @Override
    @Cacheable(value = "getAllCompanies" ,cacheResolver = "cacheResolver" )
    public List<Company> getAllCompanies() {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        List<Company> companies =   companyRepo.findAll().stream().map(item->{
            Company comp = new Company();
            BeanUtils.copyProperties(item,comp);
            return comp;
        }).collect(Collectors.toList());

        return companies;

    }
}
