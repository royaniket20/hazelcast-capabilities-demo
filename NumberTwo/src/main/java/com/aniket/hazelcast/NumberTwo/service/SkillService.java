package com.aniket.hazelcast.NumberTwo.service;


import com.aniket.hazelcast.NumberTwo.dto.Skill;

import java.util.List;

public interface SkillService {

    public Skill updateRandomSkill(String skillId,Skill skill);
    public List<Skill> getAllSkills();
}
