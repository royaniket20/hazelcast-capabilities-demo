package com.aniket.hazelcast.NumberTwo.controller;

import com.aniket.hazelcast.NumberTwo.dto.Company;
import com.aniket.hazelcast.NumberTwo.dto.Skill;
import com.aniket.hazelcast.NumberTwo.dto.SystemInfo;
import com.aniket.hazelcast.NumberTwo.service.CompanyService;
import com.aniket.hazelcast.NumberTwo.service.SkillService;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Date;

@RestController
@RequestMapping("/NumberTwo")
public class NumberTwoController {

    @Autowired
    private SkillService skillService;
    @Autowired
    private CompanyService companyService;


    @GetMapping("/allSkills")
    public ObjectNode getAllSkills() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setDateFormat(new SimpleDateFormat("dd-MM-yyyy"));
        ObjectNode rootNode = mapper.createObjectNode();
        SystemInfo systemInfo = systemInfo();
        rootNode.set("systemInfo", mapper.convertValue(systemInfo, JsonNode.class));
        rootNode.set("data", mapper.convertValue(skillService.getAllSkills(), JsonNode.class));
        return rootNode;


    }

    @GetMapping("/allCompanies")
    public ObjectNode getAllCompanies() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setDateFormat(new SimpleDateFormat("dd-MM-yyyy"));
        ObjectNode rootNode = mapper.createObjectNode();
        SystemInfo systemInfo = systemInfo();
        rootNode.set("systemInfo", mapper.convertValue(systemInfo, JsonNode.class));
        rootNode.set("data", mapper.convertValue(companyService.getAllCompanies(), JsonNode.class));
        return rootNode;


    }


    @PutMapping("/updateCompany/{id}")
    public ObjectNode updateEmployee(@PathVariable String id , @RequestBody Company company) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setDateFormat(new SimpleDateFormat("dd-MM-yyyy"));
        ObjectNode rootNode = mapper.createObjectNode();
        SystemInfo systemInfo = systemInfo();
        rootNode.set("systemInfo", mapper.convertValue(systemInfo, JsonNode.class));
        rootNode.set("data", mapper.convertValue(companyService.updateCompany(id,company), JsonNode.class));
        return rootNode;


    }

    @PutMapping("/updateSkill/{id}")
    public ObjectNode updateEmployee(@PathVariable String id , @RequestBody Skill skill) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setDateFormat(new SimpleDateFormat("dd-MM-yyyy"));
        ObjectNode rootNode = mapper.createObjectNode();
        SystemInfo systemInfo = systemInfo();
        rootNode.set("systemInfo", mapper.convertValue(systemInfo, JsonNode.class));
        rootNode.set("data", mapper.convertValue(skillService.updateRandomSkill(id,skill), JsonNode.class));
        return rootNode;


    }

    private SystemInfo systemInfo() {
        SystemInfo systemInfo = new SystemInfo();
        try {
            InetAddress ip = InetAddress.getLocalHost();
            systemInfo.setIpAddress(ip.toString());
            String hostname = ip.getHostName();
            systemInfo.setHostName(hostname);

            InetAddress localHost = InetAddress.getLocalHost();
            NetworkInterface ni = NetworkInterface.getByInetAddress(localHost);
            byte[] hardwareAddress = ni.getHardwareAddress();

            String[] hexadecimal = new String[hardwareAddress.length];
            for (int i = 0; i < hardwareAddress.length; i++) {
                hexadecimal[i] = String.format("%02X", hardwareAddress[i]);
            }
            String macAddress = String.join("-", hexadecimal);
            systemInfo.setMacAddress(macAddress);
        } catch (UnknownHostException | SocketException e) {
            systemInfo.setMacAddress("ERROR");
            systemInfo.setIpAddress("ERROR");
            systemInfo.setHostName("ERROR");
        } finally {
                systemInfo.setDate(new Date());
        }
        return systemInfo;
    }


}
