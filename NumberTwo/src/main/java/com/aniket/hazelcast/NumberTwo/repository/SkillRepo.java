package com.aniket.hazelcast.NumberTwo.repository;


import com.aniket.hazelcast.NumberTwo.entity.SkillEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SkillRepo extends JpaRepository<SkillEntity,String> {
}
