package com.aniket.hazelcast.NumberTwo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class NumberTwoApplication {

	public static void main(String[] args) {
		SpringApplication.run(NumberTwoApplication.class, args);
	}

}
