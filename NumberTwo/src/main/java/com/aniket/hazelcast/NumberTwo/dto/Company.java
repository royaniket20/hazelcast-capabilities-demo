package com.aniket.hazelcast.NumberTwo.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
    @AllArgsConstructor
    @NoArgsConstructor
    @Builder

    public class Company implements Serializable {
    private static final long serialVersionUID = 1L;
        private String id;


        private String name;


        private String industry;

        private String address;


    }