package com.aniket.hazelcast.NumberTwo.service;


import com.aniket.hazelcast.NumberTwo.dto.Company;
import com.aniket.hazelcast.NumberTwo.dto.Skill;

import java.util.List;

public interface CompanyService {

    public Company updateCompany(String compId, Company company);
    public List<Company> getAllCompanies();
}
